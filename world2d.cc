#include "world2d.h"
#include <cstdlib>
#include <algorithm>

// Para imprimir informacion
// #define DEBUG

World2d::World2d(uint dimension, bool emitARMS)
                : eArms(emitARMS), dim(dimension)
{
  // Crear el mundo
  theWorld = new RobotPtr*[dimension]();
  for (size_t i = 0; i < dimension; ++i)
  {
    theWorld[i] = new RobotPtr[dimension]();
  }
}

World2d::~World2d()
{
  // Liberar memoria de todos los robots contenidos
  freeRobotsMemory();

  // Liberar memoria del mundo
  for (size_t i = 0; i < dim; ++i)
  {
    delete[] theWorld[i];
    theWorld[i] = nullptr;
  }
  delete[] theWorld;
  theWorld = nullptr;
}

void World2d::addRobot (RobotPtr r)
{
  // Recorrer el mapa en busca de robots y los almacena en un vector
  std::vector<RobotPtr> inRobots;
  for (size_t i = 0; i < dim; i++) {
    for (size_t j = 0; j < dim; j++) {
      if (theWorld[i][j] != nullptr){
        inRobots.push_back(theWorld[i][j]);
      }
    }
  }

  // Buscar si el robot ya pertenece al mapa
  auto aux = std::find(inRobots.begin(), inRobots.end(), r);
  auto aux2 = std::find(outRobots.begin(), outRobots.end(), r);

  // Si el robot esta en el mundo (dentro o fuera), no anadirlo
  if (not (aux == inRobots.end() && aux2 == outRobots.end())){
    robotNotAddedSignal(r);
    return;
  }
  else
  {
    // Hacer que el mundo del robot sea este
    r->setWorld2d(this);
    // Conectar la senial moveSignal al slot onRobotMoved
    r->moveSignal.connect(boost::bind(&World2d::onRobotMoved, this, _1));
    // Si el robot colisiona con otro o esta fuera, sacarlo del tablero
    if (robotOutOfBounds(r) || theWorld[r->getY()][r->getX()]){
      outRobots.push_back(r);
      robotNotAddedSignal(r);
    }
    else{
      theWorld[r->getY()][r->getX()] = r;
      robotAddedSignal(r);
    }
  }
}

void World2d::moveRobots (uint maxLoops)
{
  for (size_t i = 0; (i < maxLoops) && aliveRobots(); i++) {

    // Recorrer el mapa en busca de robots y los almacena en un vector
    std::vector<RobotPtr> inRobots;
    for (size_t i2 = 0; i2 < dim; i2++) {
      for (size_t j = 0; j < dim; j++) {
        if (theWorld[i2][j] != nullptr){
          inRobots.push_back(theWorld[i2][j]);
        }
      }
    }

    for (auto it : inRobots){
      moveRobot(it);
    }

    // Mover los robots que estan fuera del mapa -- no se mueven!!
    // for (auto it : outRobots){
    //   moveRobot(it);
    // }

    // Signal
    if (eArms)
      allRobotsMovedSignal(this);
  }
}

void World2d::showWorld ()
{
  for (size_t i = 0; i < dim; ++i)
  {
    for (size_t j = 0; j < dim; ++j)
    {
      char c = (theWorld[i][j]) ? theWorld[i][j]->getName()[0] : NOROBOT;
      std::cout << c;
    }
    std::cout << std::endl;
  }
}

void World2d::showOutRobots ()
{
  // Recorrer todo el vector e imprimirlo por pantalla
  for (auto robot : outRobots)
  {
    std::cout << *robot;
    std::cout << std::endl;
  }
}

bool World2d::existsRobot (RobotPtr r, uint& rx, uint& ry)
{
  bool found = false;

  for (size_t i = 0; (i < dim) && (found == false); ++i){
    for (size_t j = 0; (j < dim) && (found == false); ++j){
      if (theWorld[i][j] == r){
        rx = j;
        ry = i;
        found = true;
      }
    }
  }

  return found;
}

void World2d::moveRobot (RobotPtr r)
{
  // #undef RAND_MAX
  // #define RAND_MAX 3
  uint dir = random() % 4;
  // #undef RAND_MAX
  // #define RAND_MAX dim/2
  int q = random() % (dim/2) + 1;
  switch(dir)
  {
    case LEFT:
      r->left(q);
      break;
    case RIGHT:
      r->right(q);
      break;
    case UP:
      r->up(q);
      break;
    case DOWN:
      r->down(q);
      break;
    default:
    #ifdef DEBUG
      std::cout << "El robot no se ha movido" << std::endl;
    #endif
      break;
  }
  // #undef RAND_MAX
}

bool World2d::robotOutOfBounds (RobotPtr r)
{
  return (r->getX() < 0 || r->getX() >= static_cast<int>(dim) ||
          r->getY() < 0 || r->getY() >= static_cast<int>(dim));
}

bool World2d::aliveRobots ()
{
  for (size_t i = 0; i < dim; i++) {
    for (size_t j = 0; j < dim; j++) {
      if (theWorld[i][j]){
        return true;
      }
    }
  }

  return false;
}

void World2d::freeRobotsMemory ()
{
  // Liberar memoria de los robots del tablero
  for (size_t i = 0; i < dim; i++) {
    for (size_t j = 0; j < dim; j++) {
      if (theWorld[i][j] != nullptr){
        delete theWorld[i][j];
        theWorld[i][j] = nullptr;
      }
    }
  }

  // Liberar memoria de los robots de fuera
  for (auto it : outRobots){
    delete it;
    it = nullptr;
  }
  outRobots.clear();
}

void World2d::onRobotMoved (RobotPtr r)
{
  // Buscar el robot en vector de robots salidos
  auto robot = std::find(outRobots.begin(), outRobots.end(), r);

  // Si el robot estaba dentro del mapa
  if (robot == outRobots.end())
  {
    // Si el robot se ha salido del mapa
    if (robotOutOfBounds(r))
    {
      // Buscar al robot en el mapa y eliminarlo
      bool bFound = false;
      for (size_t i = 0; i < dim &&  bFound == false; i++) {
        for (size_t j = 0; j < dim && bFound == false; j++) {
          if (theWorld[i][j] == r) {
            bFound = true;
            theWorld[i][j] = nullptr;
          }
        }
      }
      outRobots.push_back(r);
    }
    // Si el robot se ha movido dentro del mismo mapa
    else
    {
      // Borrar su posicion antigua
      // Buscar al robot en el mapa y eliminarlo

      bool bFound = false;
      for (size_t i = 0; i < dim &&  bFound == false; i++) {
        for (size_t j = 0; j < dim && bFound == false; j++) {
          if (theWorld[i][j] == r) {
            bFound = true;
            theWorld[i][j] = nullptr;
            break;
          }
          if (bFound) {break;}
        }
      }
      // Si el robot se ha movido a una posicion ocupada
      if (theWorld[r->getY()][r->getX()])
      {
        // mover el robot a los robots salidos
        outRobots.push_back(r);
      }
      else
        // Actualizar la nueva
        theWorld[r->getY()][r->getX()] = r;
    }
  }
  // Si el robot estaba fuera del mapa -- NO SE HACE NADA (practica mal leida)
  //   else
  //   {
  //     // Si sigue fuera del mapa, no hacer nada
  //
  //     // En cambio si entra al mapa, actualizar su posicion
  //     if (not robotOutOfBounds(r))
  //     {
  //       theWorld[r->getY()][r->getX()] = r;
  //
  //       // Y eliminarlo del vector
  //       std::vector<RobotPtr>::iterator it;
  //       for (it = outRobots.begin(); it != outRobots.end(); ++it){
  //         if (*it == r){
  //           break;
  //         }
  //       }
  //       outRobots.erase(it);
  //     }
  //   }
}
