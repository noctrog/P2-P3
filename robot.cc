#include "world2d.h"
#include "robot.h"

Robot::Robot(std::string _name, int x, int y, World2dPtr _w)
      : name(_name), w(_w)
{
  p.x = x;
  p.y = y;

  if (w)
    w->addRobot(this);
}

Robot::~Robot()
{

}

std::string Robot::directionToString (Direction d)
{
  switch (d)
  {
    case LEFT:
      return std::string{"LEFT"};
      break;
    case RIGHT:
      return std::string{"RIGHT"};
      break;
    case UP:
      return std::string{"UP"};
      break;
    case DOWN:
      return std::string{"DOWN"};
      break;
    default:
      return std::string{"UNKNOWN DIRECTION"};
      break;
  }
}

std::string Robot::getName ()
{
  nameGetSignal(name);
  return name;
}

void Robot::setWorld2d(World2dPtr _w)
{
  if (w != _w && _w)
  {
    w = _w;
    worldChangeSignal(this);
  }
}

void Robot::left  (int q)
{
  p.x -= q;

  //Emitir moveSignal
  moveSignal(this);
  //Emitir lefttSignal
  leftSignal(this, q);
}
void Robot::right (int q)
{
  p.x += q;

  //Emitir moveSignal
  moveSignal(this);
  //Emitir rightSignal
  rightSignal(this, q);
}
void Robot::up    (int q)
{
  p.y -= q;

  //Emitir moveSignal
  moveSignal(this);
  //Emitir upSignal
  upSignal(this, q);
}
void Robot::down  (int q)
{
  p.y += q;

  //Emitir moveSignal
  moveSignal(this);
  //Emitir upSignal
  downSignal(this, q);
}

ostream& operator<< (ostream& os, Robot& r)
{
  os << "Name: " << r.getName() << std::endl;
  os << "Y: " << r.getY() << " X: " << r.getX();

  return os;
}
